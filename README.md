# SAMWAD

Samwad (meaning conversation in Sanskrit) is an video-conferencing solution built on top of Kurento media player.

## Introduction
Samwad is created with the aim of making it simple to use in AR/VR settings or otherwise.  
The app features Opencv filters suitable for application in AR/VR meeting solutions.

*Filters:*
    * TrackIt : This is useful to mark regions in the video with a real-time marker using the cursor.


