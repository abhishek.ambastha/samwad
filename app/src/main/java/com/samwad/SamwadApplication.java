package com.samwad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamwadApplication {

    public static void main(String[] args) {
        SpringApplication.run(SamwadApplication.class);
    }

}
