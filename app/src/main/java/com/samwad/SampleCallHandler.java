package com.samwad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SampleCallHandler extends TextWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(SampleCallHandler.class);

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        logger.info("Message Received {}", message.getPayload());
        session.sendMessage(new TextMessage("Hello Brother!"));
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info("Connection established");
    }
}
