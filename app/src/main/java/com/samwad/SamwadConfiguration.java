package com.samwad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.kurento.client.KurentoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class SamwadConfiguration implements WebSocketConfigurer {

    @Bean
    public SampleCallHandler sampleCallHandler() {
        return new SampleCallHandler();
    }

    @Bean
    public SingleCallHander singleCallHander() {
        return new SingleCallHander();
    }

    @Bean
    public KurentoClient getKurentoClient(){
        return KurentoClient.create();
    }

    @Bean UserRegistry getUserRegistry(){
        return new UserRegistry();
    }

    @Bean
    public Gson getGson(){
        return new GsonBuilder().create();
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(sampleCallHandler(), "/sample");
        registry.addHandler(singleCallHander(), "/call");
    }
}
