package com.samwad;

import com.google.gson.Gson;
import org.kurento.client.KurentoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private KurentoClient client;

    @Autowired
    private Gson gson;

    @RequestMapping("/control")
    public String hello() {
        logger.info("Testing controller");
        logger.info("Beans {} \n {}", client, gson);
        return "Hello there!!!";
    }
}
