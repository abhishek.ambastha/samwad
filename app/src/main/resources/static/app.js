
var ws = new WebSocket('wss://' + location.host + '/call');
var videoInput;
var videoOutput;
var videoProcessed;
var webRtcPeer;

var registerState = null;
NOT_REGISTERED = 0;
REGISTERING = 1;
REGISTERED = 2;

var callState = null;
NO_CALL = 0;
PROCESSING_CALL = 1;
IN_CALL = 2;

var chanId = 0;

function getChannelName () {
    return "TestChannel" + chanId++;
}

function register() {
    var name = document.getElementById('name').value;
    if (name === '') {
        window.alert('You must insert your user name');
        return;
    }

    setRegisterState(REGISTERING);

    var message = {
        id : 'register',
        name: name
    };

    sendMessage(message);

    console.log('Registered: ' + name);
}

function call() {
    if (document.getElementById('peer').value === '') {
        window.alert('You must specify the peer name');
        return;
    }
    setCallState(PROCESSING_CALL);

    var dataChannelSend = document.getElementById("dataChannelSend");

    var sendButton = document.getElementById("send");
        sendButton.addEventListener("click", function() {
            var data = dataChannelSend.value;
            console.log("Send button pressed. Sending data " + data);
            webRtcPeer.send(data);
            dataChannelSend.value = "";
    });

    function onOpen(event) {
        dataChannelSend.disabled = false;
        enableButton("send");
    }

    function onClosed(event) {
        dataChannelSend.disabled = true;
        enableButton("send");
    }

    var options = {
        localVideo : videoInput,
        remoteVideo : videoOutput,
        dataChannelConfig: {
            id : getChannelName(),
            onopen : onOpen,
            onclose : onClosed
        },
        dataChannels : true,
        onicecandidate : onIceCandidate,
        onerror : onError
    };

    webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options,
        function(error) {
            if (error) {
                return console.error(error);
            }
            webRtcPeer.generateOffer(onOfferCall);
        });
}

function onIceCandidate(candidate) {
    console.log("Local candidate" + JSON.stringify(candidate));

    var message = {
        id : 'onIceCandidate',
        candidate : candidate
    };
    sendMessage(message);
}

function onError() {
    setCallState(NO_CALL);
}

function onOfferCall(error, offerSdp) {
    if (error)
        return console.error('Error generating the offer');
    console.log('Invoking SDP offer callback function');
    var message = {
        id : 'call',
        from : document.getElementById('name').value,
        to : document.getElementById('peer').value,
        sdpOffer : offerSdp
    };
    sendMessage(message);
}

function stop() {
    console.log('Stopping ... ');
    setCallState(NO_CALL);
    if (webRtcPeer) {
        webRtcPeer.dispose();
        webRtcPeer = null;

        if (!message) {
            var message = {
                id : 'stop'
            };
            sendMessage(message);
        }
    }
}

function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Sending message: '+ jsonMessage);
    ws.send(jsonMessage);
}

function setCallState(nextState) {
    switch (nextState) {
        case NO_CALL:
            enableButton("call");
            break;
        case PROCESSING_CALL:
            disableButton("call");
            break;
        case IN_CALL:
            disableButton("call");
            break;
        default:
            return;
    }
    callState = nextState;
}

function disableButton(id) {
    var element = document.getElementById(id);
    element.disabled = true;
}

function enableButton(id) {
    var element = document.getElementById(id);
    element.disabled = false;
}

function setRegisterState(nextState) {
    switch (nextState) {
        case NOT_REGISTERED:
            enableButton('register');
            setCallState(NO_CALL);
            break;
        case REGISTERING:
            disableButton('register');
            break;
        case REGISTERED:
            disableButton('register');
            setCallState(NO_CALL);
            break;
        default:
            return;
    }
    registerState = nextState;
}

function registerResponse(message) {
    if (message.response === 'accepted') {
        setRegisterState(REGISTERED);
    } else {
        setRegisterState(NOT_REGISTERED);
        var errorMessage = message.message ? message.message
            : 'Unknown reason for register rejection.';
        console.log(errorMessage);
        alert('Error registering user. See console for further information.');
    }
}

function callResponse(message) {
    if (message.response !== 'accepted') {
        console.info('Call not accepted by peer. Closing call');
        var errorMessage = message.message ? message.message
            : 'Unknown reason for call rejection.';
        console.log(errorMessage);
        window.alert(message.data);
        stop();
    } else {
        setCallState(IN_CALL);
        webRtcPeer.processAnswer(message.sdpAnswer, function(error) {
            if (error)
                return console.error(error);
        });
    }
}

function incomingCall(message) {
    // If bussy just reject without disturbing user
    if (callState !== NO_CALL) {
        var incomingResponse = {
            id : 'incomingCallResponse',
            from : message.from,
            callResponse : 'reject',
            message : 'bussy'
        };
        return sendMessage(incomingResponse);
    }

    setCallState(PROCESSING_CALL);
    if (confirm('User ' + message.from
        + ' is calling you. Do you accept the call?')) {

        from = message.from;

        var options = {
            localVideo : videoInput,
            remoteVideo : videoOutput,
            onicecandidate : onIceCandidate,
            onerror : onError
        };
        webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options,
            function(error) {
                if (error) {
                    return console.error(error);
                }
                webRtcPeer.generateOffer(onOfferIncomingCall);
            });

    } else {
        var response = {
            id : 'incomingCallResponse',
            from : message.from,
            callResponse : 'reject',
            message : 'user declined'
        };
        sendMessage(response);
        stop();
    }
}

function onOfferIncomingCall(error, offerSdp) {
    if (error)
        return console.error("Error generating the offer");
    var response = {
        id : 'incomingCallResponse',
        from : from,
        callResponse : 'accept',
        sdpOffer : offerSdp
    };
    sendMessage(response);
}

function startCommunication(message) {
    setCallState(IN_CALL);
    webRtcPeer.processAnswer(message.sdpAnswer, function(error) {
        if (error)
            return console.error(error);
    });
}

ws.onmessage = function (message) {
    var parsedMessage = JSON.parse(message.data);
    console.log('Received from spring: ' + message.data);

    switch (parsedMessage.id) {
        case 'registerResponse':
            registerResponse(parsedMessage);
            break;
        case 'callResponse':
            callResponse(parsedMessage);
            break;
        case 'incomingCall':
            incomingCall(parsedMessage);
            break;
        case 'startCommunication':
            startCommunication(parsedMessage);
            break;
        case 'stopCommunication':
            console.info('Communication ended by remote peer');
            stop(true);
            break;
        case 'iceCandidate':
            webRtcPeer.addIceCandidate(parsedMessage.candidate, function(error) {
                if (error)
                    return console.error('Error adding candidate: ' + error);
            });
            break;
        default:
            console.error('Unrecognized message', parsedMessage);
    }
};


window.onload = function () {
    console.log('Hey!! I am loaded!!');
    videoInput = document.getElementById('inputVideo');
    videoOutput = document.getElementById('outputVideo');
    videoProcessed = document.getElementById('processedVideo');
};